package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRequest(t *testing.T) {
	tests := []struct {
		name               string
		path               string
		expectedHTTPStatus int
		expectedResponse   string
	}{
		{
			name:               "emtpy path",
			path:               "",
			expectedHTTPStatus: http.StatusOK,
			expectedResponse:   "Hello World\nYou requested: \nthis is request number 0\n",
		},
		{
			name:               "path '/info'",
			path:               "info",
			expectedHTTPStatus: http.StatusOK,
			expectedResponse:   "Hello World\nYou requested: info\nthis is request number 1\n",
		},
	}

	for _, data := range tests {
		t.Run(data.name, func(t *testing.T) {
			request := newGetScoreRequest(data.path)
			response := httptest.NewRecorder()

			handler(response, request)

			assertStatus(t, response.Code, data.expectedHTTPStatus)
			assertResponseBody(t, response.Body.String(), data.expectedResponse)
		})
	}
}

func newGetScoreRequest(path string) *http.Request {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("%s", path), nil)
	return req
}

func assertStatus(t *testing.T, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("did not get correct status, got %d, want %d", got, want)
	}
}

func assertResponseBody(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("response body is wrong, got %q want %q", got, want)
	}
}
